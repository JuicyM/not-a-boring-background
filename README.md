<p align="center">
     <img src="header.gif" />
</p>

<h2 align="center">Change your Discord background to ANYTHING (I think...)</h2>

<hr>
<p align="center">
    <img src="livedemo_640.gif"/><br>
    <a href="//gitlab.com/aldroyd/not-a-boring-background/raw/master/html5.theme.css">Use the theme file</a>. <a href="//html5wallpaper.com">You can try some wallpapers examples here</a>.
</p>

<p align="center">
    <strong>NBB</strong> uses a 100% <a href="https://www.w3schools.com/html/html_iframe.asp">iframe</a> display as background, which is defined inside a theme file.
</p>

<h3>How to change your background</h3>

<ul style="list-style: none">
    <li>Save the theme <a href="https://gitlab.com/aldroyd/not-a-boring-background/raw/master/html5.theme.css">example file</a> on your themes folder <strong>(%appdata%/BetterDiscord/themes)</strong></li>
    <li>Access <a href="http://html5wallpaper.com/">HTML5Wallpaper</a></li>
    <li>Pick one</li>
    <li>Copy the direct link (a greyed box aside a <strong>COPY</strong> button)</li>
    <li>In your Discord, click <strong>NBB</strong> button on the top of your client</li>
    <li>If you want a <strong>YouTube video</strong>, right-click on <strong>NBB</strong> to switch mode. Then, click again to insert the video.</li>    
    <li>If you want a <strong>IMAGE/GIF</strong>, right-click on <strong>YT</strong> to switch mode. Then, click again switch to an image or an animated GIF.</li>    
</ul>

<p>
The background will be saved automatically and will be restored the next time you open Discord.
<strong>NOTICE:</strong> Some sites, like <strong>imgur</strong>, won't work in iframe methods due to privacy policies, but can still use in <b>IMG</b> mode.
</p>

<h3>How to edit the example file</h3>

<ul style="list-style: none">
    <li>Access <a href="http://html5wallpaper.com/">HTML5Wallpaper</a></li>
    <li>Pick one</li>
    <li>Copy the direct link (a greyed box aside a <strong>COPY</strong> button)</li>
    <li>Open the <code>html5.theme.css</code> file</li>
    <li>Search for <code>var wallpaper = '//html5wallpaper.com/wp-depo/346/';</code> <strong>(LINE 97.)</strong></li>
    <li>Paste your selected background URL here. <strong>(Remove the 'http:' protocol, you need HTTPS here!)</strong></li>
    <li>Paste on your themes folder <strong>(%appdata%/BetterDiscord/themes)</strong></li>
    <li>Reload your Discord client</li>
</ul>

**Note:** There's a commented line **(39)** saying that every style **after** that line isn't required. It's true.

<h3>Required</h3>
<ul>
    <li><a href="//betterdiscord.net">BetterDiscord</a> (by Jiiks)</li>
</ul>

<h3>Credits</h3>
<ul>
    <li><a href="//html5wallpaper.com/wallpaper/flat-earth/">Flat Earth</a> (by... admin?)</li>
</ul>
